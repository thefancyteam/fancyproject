﻿/// <reference path="~/Scripts/angular.js" />
/// <reference path="~/Scripts/angular-mocks.js" />
/// <reference path="~/FancyTeamApp/Factories/fancyFactory.js" />
/// <reference path="~/Scripts/jquery-2.1.4.js" />
/// <reference path="~/Scripts/angular-route.js" />

(function () {
    "use strict";

    angular
        .module("teamFancyApp", ["ngAnimate", "ngRoute", "LocalStorageModule"])
        .config([
            "$routeProvider", function ($routeProvider) {
                $routeProvider
                    .when("/register", {
                        controller: "RegisterController",
                        templateUrl: "FancyTeamApp/partials/register.html"
                    })
                    .when("/secrets", {
                        controller: "SecretsController",
                        templateUrl: "FancyTeamApp/partials/secrets.html"
                    })
                    .when("/login", {
                        controller: "LoginController",
                        templateUrl: "FancyTeamApp/partials/login.html"
                    })
                    .when("/start", {
                        controller: "StartController",
                        templateUrl: "FancyTeamApp/partials/start.html"
                    })
                    .when("/error", {
                        templateUrl: "FancyTeamApp/partials/error.html"
                    })
                    .otherwise({ redirectTo: "/start" });
            }
        ])
        .config([
            "$httpProvider", function ($httpProvider) {
                $httpProvider.interceptors.push("authenticationInterceptorService");
            }])
        .run([
            "$rootScope", "$location", "authenticationService", "localStorageService", function ($rootScope, $location, authenticationService) {
                authenticationService.fillAuthData();

                $rootScope.currentLoggedInUser = "{ }";
                $rootScope.currentStatus = "Ok.";
                $rootScope.isUserLoggedIn = false;

                if (authenticationService.auth.isAuthenticated) {
                    $rootScope.isUserLoggedIn = true;
                    $rootScope.currentLoggedInUser = authenticationService.auth.userName;
                }

                $rootScope.$on("$locationChangeStart", function () {
                    var unrestrictedAreas = $.inArray($location.path(), ["/login", "/start", "/register", "/error"]) > -1;

                    if (!unrestrictedAreas && !authenticationService.auth.isAuthenticated) {
                        $location.path("/login");
                    };
                });
            }
        ]);
})();