﻿/// <reference path="~/Scripts/angular.js" />
/// <reference path="~/Scripts/angular-mocks.js" />

(function () {
    "use strict";

    // ReSharper disable once UnusedLocals
    var localDbEndpoint = "http://localhost:26516/";
    var registratedUsersEndpoint = "http://conceptendpoint.azurewebsites.net/";
    var authServiceEndpoint = "http://ngauthenticationapi.azurewebsites.net/";

    angular
        .module("teamFancyApp")
        .factory("authenticationService", [
            "$http", "$q", "localStorageService", function ($http, $q, localStorageService) {

                var apiRegister = "api/account/register";
                var apiSave = "api/fancy/InsertNewUser";

                function setTokenForRegUserEndpoint(loginData, accessToken) {
                    var data =
                        "?email=" + loginData.userName +
                        "&password=" + loginData.userPassword +
                        "&token=" + accessToken;

                    return $http.post(registratedUsersEndpoint + "api/fancy/Login" + data)
                        .then(function (response) {
                            return response;
                        }).catch(function (response) {
                            return response;
                        });
                }

                var authenticationServicefactory = {};

                authenticationServicefactory.auth = {
                    isAuthenticated: false,
                    userName: ""
                };

                authenticationServicefactory.saveRegistrationData = function (newUserModel) {
                    authenticationServicefactory.logOutCurrentUser();

                    return $http.post(authServiceEndpoint + apiRegister, newUserModel)
                        .then(function (response) {
                            return response;
                        });
                };

                authenticationServicefactory.saveRegistrationDataToSecondEndpoint = function (newUserModel) {
                    return $http.post(registratedUsersEndpoint + apiSave, newUserModel)
                        .then(function (response) {
                            return response;
                        });
                };

                authenticationServicefactory.loginUser = function (loginData) {
                    var data = "grant_type=password&username=" + loginData.userName +
                        "&password=" + loginData.userPassword;

                    return $http.post(authServiceEndpoint + "token", data, {
                        headers: { "Content-Type": "application/x-www-form-urlencoded" }

                    }).then(function (response) {
                        localStorageService.set("authorizationData", {
                            token: response.data.access_token,
                            userName: loginData.userName
                        });

                        authenticationServicefactory.auth.isAuthenticated = true;
                        authenticationServicefactory.auth.userName = loginData.userName;

                        setTokenForRegUserEndpoint(loginData, response.data.access_token).then(function () {
                        });

                        return response;

                    }).catch(function (response) {
                        authenticationServicefactory.logOutCurrentUser();

                        return response;
                    });

                };

                authenticationServicefactory.logOutCurrentUser = function () {
                    localStorageService.remove("authorizationData");
                    authenticationServicefactory.auth.isAuthenticated = false;
                    authenticationServicefactory.auth.userName = "";
                };

                authenticationServicefactory.fillAuthData = function () {
                    var authData = localStorageService.get("authorizationData");
                    if (authData) {
                        authenticationServicefactory.auth.isAuthenticated = true;
                        authenticationServicefactory.auth.userName = authData.userName;
                    }
                };

                return authenticationServicefactory;
            }
        ])
        .factory("credentialsService", [
            "$http", "$q", function ($http, $q) {

                var apiDelete = "api/fancy/DeleteFancyByEmail?email=";
                var apiGetall = "api/fancy/GetAllFancy";
                var token = "TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwgc2VkIG";

                var credentialsServiceFactory = {};

                credentialsServiceFactory.getcredentials = function () {
                    return $http.get(registratedUsersEndpoint + apiGetall)
                        .then(function (response) {
                            return response.data;
                        }).catch(function (response) {
                            return response;
                        });
                };

                // TODO: This is actually the classic $q anti pattern. Refactor this
                credentialsServiceFactory.removeRegistredUser = function (userMail) {
                    var deffered = $q.defer();

                    $http.post(registratedUsersEndpoint + apiDelete + userMail + "&token=" + token)
                        .then(function (response) {
                            deffered.resolve(response);
                        }).catch((function (error) {
                            deffered.reject(error);
                        }));

                    return deffered.promise;
                };

                return credentialsServiceFactory;
            }
        ])
        .factory("authenticationInterceptorService", [
            "$q", "$location", "localStorageService", function ($q, $location, localStorageService) {
                var authenticationInterceptorServiceFactory = {};

                authenticationInterceptorServiceFactory.request = function (request) {

                    console.log("Interceptor data, request: ");
                    console.log(request);

                    request.headers = request.headers || {};

                    var authData = localStorageService.get("authorizationData");
                    if (authData) {
                        request.headers.Authorization = "Bearer " + authData.token;
                    }

                    return request;
                };

                authenticationInterceptorServiceFactory.responseError = function (responseError) {
                    if (responseError.status === 401) {
                        $location.path("/error");
                    }

                    return $q.reject(responseError);
                };

                return authenticationInterceptorServiceFactory;
            }
        ]);
})();