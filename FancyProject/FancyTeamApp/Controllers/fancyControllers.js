﻿/// <reference path="~/Scripts/angular.js" />
/// <reference path="~/Scripts/jquery-2.1.4.js" />
/// <reference path="~/Scripts/angular-mocks.js" />
/// <reference path="~/FancyTeamApp/Factories/fancyFactory.js" />

(function () {
    "use strict";

    var statusDivElement;

    $(function () {
        statusDivElement = $("#divStatusElement");
    });

    var fireResponseToGui = function () {
        statusDivElement.fadeOut(800, function () {
            statusDivElement.fadeIn(800);
        });
    };

    angular
        .module("teamFancyApp")
        .controller("LoginController", [
            "$scope", "$rootScope", "$location", "authenticationService",
            function ($scope, $rootScope, $location, authenticationService) {

                $scope.loggedInUser = "";
                $scope.isUserAlreadyLoggedIn = authenticationService.auth.isAuthenticated;
                $scope.loginData = {
                    userName: "",
                    userPassword: ""
                }

                $scope.loginUserByKeyPress = function (keyPressEvent) {
                    if (keyPressEvent.which === 13) {
                        $scope.loginUser();
                    }
                }

                $scope.loginUser = function () {
                    if (authenticationService.auth.isAuthenticated) {
                        fireResponseToGui();
                        $rootScope.currentStatus = "You are already logged-in.";
                        $rootScope.isUserLoggedIn = true;
                        return;
                    }

                    authenticationService.loginUser($scope.loginData).then(function(response) {
                        if (response.status === 200) {
                            $rootScope.currentLoggedInUser = "{ " + $scope.loginData.userName + " }";
                            $rootScope.currentStatus = "Login successfull.";
                            $rootScope.isUserLoggedIn = true;
                        } else {
                            if (typeof response.data.error_description !== "undefined") {
                                $rootScope.currentStatus = "Login attempt failed: " + response.data.error_description;
                            } else {
                                $rootScope.currentStatus = "Login attempt failed: " + response.statusText;
                            }
                        }

                        fireResponseToGui();
                    });
                };

                $scope.tryingToAccessSecrets = function () {
                    if (!authenticationService.auth.isAuthenticated) {
                        fireResponseToGui();
                        $rootScope.currentStatus = "Only logged-in users can access the secrets.";
                    }
                }

                $scope.logOutUser = function () {
                    if (!authenticationService.auth.isAuthenticated) {
                        fireResponseToGui();
                        $rootScope.currentStatus = "Already logged out.";
                        return;
                    }

                    authenticationService.logOutCurrentUser();

                    fireResponseToGui();
                    $rootScope.isUserLoggedIn = false;
                    $rootScope.currentLoggedInUser = "{ }";
                    $rootScope.currentStatus = "Logged out.";

                    $location.path("/start");
                };
            }
        ])
        .controller("RegisterController", [
            "$scope", "$rootScope", "localStorageService", "authenticationService", "$timeout", "$location",
            function ($scope, $rootScope, localStorageService, authenticationService, $timeout, $location) {
                var tempCoolDown = function () {
                    var timer = $timeout(function () {
                        $timeout.cancel(timer);
                        $location.path("/login");
                    }, 3000);
                }

                $scope.isNewAccountSuccessfull = false;
                $scope.registerNewUserModel = {
                    firstName: "",
                    lastName: "",
                    userName: "",
                    password: "",
                    confirmPassword: ""
                };

                $scope.registerNewUser = function () {
                    if (authenticationService.auth.isAuthenticated) {
                        $rootScope.currentStatus = "You are logged-in; cant register a new account. Please log-out first.";
                        $rootScope.isUserLoggedIn = true;

                        fireResponseToGui();
                        return;
                    }

                    authenticationService.saveRegistrationData($scope.registerNewUserModel).then(function () {
                        authenticationService.saveRegistrationDataToSecondEndpoint($scope.registerNewUserModel)
                          .then(function (response) {
                              console.log("Registration data feedback: ");
                              console.log(response);
                          });

                        fireResponseToGui();
                        $rootScope.currentStatus = "Your account was successfully created! You will be redirected to login page in 3 seconds!";
                        $scope.isNewAccountSuccessfull = true;

                        tempCoolDown();

                    }).catch(function (response) {
                        var errorList = [];
                        $.each(response.data.modelState, function (index, value) {
                            errorList.push(value);
                        });

                        fireResponseToGui();
                        $rootScope.currentStatus = response.statusText + ": " + errorList.join(" ");
                    });
                };
            }
        ])
        .controller("SecretsController", [
            "$scope", "$rootScope", "localStorageService", "credentialsService",
            function ($scope, $rootScope, localStorageService, credentialsService) {
                $scope.registredUsers = "";

                $scope.getRegistredUsers = function ($event) {
                    var currentButton = $($event.target);
                    currentButton.attr("disabled", true);
                    currentButton.val("Loading...");

                    credentialsService.getcredentials().then(function (response) {
                        if (response.status === 401) {
                            $rootScope.currentStatus = "Bad request: " + response.statusText;
                        } else {
                            $rootScope.currentStatus = "Loaded credental data";
                            $scope.registredUsers = response;
                        }

                        currentButton.attr("disabled", false);
                        currentButton.val("Load");

                        fireResponseToGui();
                    });
                };

                $scope.deleteUser = function (userName, $event) {
                    credentialsService.removeRegistredUser(userName).then(function (response) {

                        var currentButton = $($event.target);
                        currentButton.attr("disabled", true);

                        if (response.data.Feedback === "Error") {
                            $rootScope.currentStatus = "There was a error raised: " + response.data.Message;
                        } else {
                            $rootScope.currentStatus = "User sucessfully removed.";
                            var currentRow = $event.target.closest("tr");
                            $(currentRow).fadeOut(1500, function () {
                                currentRow.remove();
                            });
                        };

                        fireResponseToGui();

                    }).catch(function (response) {
                        $rootScope.currentStatus = "Invalid request: " + response.status + ". The response is: " + response.statusText;
                        fireResponseToGui();
                    });
                };
            }
        ])
        .controller("StartController", ["$scope", "$rootScope", function ($scope) {
            $scope.welcomeMessage = "Yes, this is a welcome message. Welcome ... !";
            $scope.filter = {};
            $scope.filter.filterByString = "";
        }]);
})();