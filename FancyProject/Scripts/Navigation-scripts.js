﻿$(function () {
    var mainContainer = $('.main-container');

    var currentPage = location.pathname.substring(1);
    $('.links').each(function() {
        if ($(this).data('page') == currentPage) {
            $(this).addClass('active-page');
        }
    });
        
    
    $('.page-name').text(currentPage.substring(0, currentPage.indexOf('.')));

    if (currentPage == 'Login.html') {
        $('#log-in').css('display', 'none');
    }

    $('.nav li').hover(function () {
        $('.page-name').text($(this).data('name'));
    },
    function () {
        $('.page-name').text(currentPage.substring(0, currentPage.indexOf('.')));
    });

    $('.menu-button').click(function () {
        mainContainer.toggleClass('active-menu');
        if ($(this).hasClass('fa-arrow-circle-down')) {
            $(this).removeClass('fa-arrow-circle-down');
            $(this).addClass('fa-arrow-circle-up');
        } else {
            $(this).removeClass('fa-arrow-circle-up');
            $(this).addClass('fa-arrow-circle-down');
        }
    });
});