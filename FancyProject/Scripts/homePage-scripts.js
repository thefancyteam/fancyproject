﻿$(function () {

    if ($(window).scrollTop() == 0) {
        $('.arrow-up').css({
            'z-index': '0'
        });
    }
    if ($(window).scrollTop() >= 550) {
        $('.image-container').addClass('animated fadeInUp');
        $('.login-link, .arrow-up').css({
            'opacity': '.7'
        });
    }

    if ($(window).scrollTop() >= 266) {
        $('article').css({ 'opacity': '1' });
        $('article').addClass('animated fadeInUp');
    }

    $('.arrow-down').click(function () {
        $('html, body').animate({
            scrollTop: 735
        }, 3000);
        return false;
    });

    $('.arrow-up').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 3000);
        return false;
    });

    $(window).scroll(function () {
        var wScroll = $(this).scrollTop();
        if (wScroll >= 2) {
            $('.arrow-down').removeClass('fadeInDown animated');
            $('.midd-div').removeClass('fadeInDown animated');
            $('.back-div').removeClass('fadeInDown animated');
        }

        $('.midd-div').css({
            'transform': 'translate(0px, ' + wScroll / 2 + '%)'
        });

        $('.arrow-down').css({
            'transform': 'translate(0px, ' + wScroll / 5 + '%)'
        });

        $('.back-div').css({
            'transform': 'translate(0px, ' + wScroll / 4 + '%)'
        });

        //$('.fore-div').css({
        //    'transform': 'translate(0px, -' + wScroll / 40 + '%)'
        //});


        if (wScroll >= 266) {
            $('article').css({
                'opacity': '1'
            });
            $('article').addClass('animated fadeInUp');
        }

        if (wScroll >= 550) {
            $('.login-link, .arrow-up').css({
                'opacity': '.7',
                'z-index': '10'
            });
            $('.image-container').addClass('animated fadeInUp');
            //$('.login-link, arrow-up').css({
            //    'opacity': '.7'
            //});
        }

        if (wScroll < 600) {
            $('.login-link, .arrow-up').css({
                'opacity': '0',
                'z-index': '0'
            });

        }
    });
});